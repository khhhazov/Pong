#include <stdio.h>

void Start();
void Field();
int Racket1(char change, int y, int height);
int Racket2(char change, int y, int height);
int BallMoveY(int y, int height, int direction_ball_y);
int BallMoveX(int x, int width, int direction_ball_x);
int Collision(int xball, int yball, int racket1_x, int racket1_y,
int racket2_x, int racket2_y, int direction_ball_x);

int main(void) {
    Start();
    return 0;
}

void Start() {
    int width = 80, height = 25;
    int player1_score = 0, player2_score = 0;
    int direction_ball_y = 1, direction_ball_x = 1;

    while (player1_score < 21 || player2_score < 21) {
        int xball = width / 2, yball = (height / 2) + 1;
        int racket1_x = 3, racket1_y = (height / 2) + 1;
        int racket2_x = width - 4, racket2_y = (height / 2) + 1;
        char move_racket, trash;

        direction_ball_y *= -1;
        direction_ball_x *= -1;
        if (player1_score == 21) {
            printf("Player 1 WIN!!!");
            break;
        }
        if (player2_score == 21) {
            printf("Player 2 WIN!!!");
            break;
        }
        while (1) {
            Field(width, height, xball, yball,
                racket1_x, racket1_y, racket2_x, racket2_y, player1_score, player2_score);
            scanf("%c%c", &move_racket, &trash);
            racket1_y = Racket1(move_racket, racket1_y, height);
            racket2_y = Racket2(move_racket, racket2_y, height);
            direction_ball_x = Collision(xball, yball, racket1_x, racket1_y,
            racket2_x, racket2_y, direction_ball_x);
            direction_ball_y = BallMoveY(yball, height, direction_ball_y);
            yball += direction_ball_y;
            direction_ball_x = BallMoveX(xball, width, direction_ball_x);
            xball += direction_ball_x;
            if (xball == 0) {
                player2_score++;
                break;
            } else if (xball == width - 1) {
                player1_score++;
                break;
            } else {
                continue;
            }
       }
    }
}

int Collision(int xball, int yball, int racket1_x, int racket1_y,
 int racket2_x, int racket2_y, int direction_ball_x) {
    if (xball == racket1_x + 1 && (yball == racket1_y
    || yball == racket1_y + 1 || yball == racket1_y - 1)) {
        return direction_ball_x * -1;
    } else if (xball == racket2_x - 1 && (yball == racket2_y
    || yball == racket2_y + 1 || yball == racket2_y - 1)) {
        return direction_ball_x * -1;
    } else {
        return direction_ball_x;
    }
}

int BallMoveY(int y, int height, int direction_ball_y) {
    if (y == 1 || y == height) {
        return direction_ball_y * -1;
    }
    return direction_ball_y;
}

int BallMoveX(int x, int width, int direction_ball_x) {
    if (x == 0 || x == width - 1) {
        return direction_ball_x * -1;
    }
    return direction_ball_x;
}

int Racket1(char change, int y, int height) {
    if (y - 1 != 1 && (change == 'a' || change == 'A')) {
        return y - 1;
    } else if (y + 1 != height && (change == 'z' || change == 'Z')) {
        return y + 1;
    } else {
        return y;
    }
}

int Racket2(char change, int y, int height) {
    if (y-1 != 1 && (change == 'k' || change == 'K')) {
        return y - 1;
    } else if (y + 1 != height && (change == 'm' || change == 'M')) {
        return y + 1;
    } else {
        return y;
    }
}

void Field(int width, int height, int xball, int yball,
 int racket1_x, int racket1_y, int racket2_x, int racket2_y,
 int player1_score, int player2_score) {
    char border = '#';

    width += 1;
    height += 2;

    for (int i = 0; i < height; i++) {
        printf("%c", border);
        for (int j = 0; j < width; j++) {
            if (i == 0 || i == (height - 1)) {
                printf("%c", border);
            } else {
                if (j == xball && i == yball) {
                    printf("@");
                } else if ((j == racket1_x && i == racket1_y)
                || (j == racket2_x && i == racket2_y)) {
                    printf("|");
                } else if ((j == racket1_x && i == racket1_y - 1)
                || (j == racket2_x && i == racket2_y - 1)) {
                    printf("|");
                } else if ((j == racket1_x && i == racket1_y + 1)
                    || (j == racket2_x && i == racket2_y + 1)) {
                    printf("|");
                } else if (j == (width / 2)) {
                    printf("|");
                } else if (i == 3 && j == width / 2 - 5) {
                    printf("%d", player1_score);
                    if (player1_score > 9)
                        j++;
                } else if (i == 3 && j == width / 2 + 5) {
                    printf("%d", player2_score);
                    if (player2_score > 9)
                        j++;
                } else if (j < (width - 1)) {
                    printf(" ");
                } else {
                    printf("%c", border);
                }
            }
        }
        printf("\n");
    }
}
